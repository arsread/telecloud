#!/bin/bash

if [ ! -f config ];then
	echo "config not exist!"
	exit 1
fi

source ./config 2>/dev/null
source util/util.sh 2>/dev/null

if [ -d data/$topic ];then
	echo "folder exist!"
	exit 1
fi

echo "Preparing..."
mkdir data/$topic
mkdir data/$topic/call_info
mkdir data/$topic/overhead
mkdir data/$topic/raw
mkdir data/$topic/remapping

echo "Compling faults..."
deploy "cd $base_dir/fault_injection/fault_repository;make"

#echo "Warming up Clearwater"
#restart_cleatwater
#expect -f util/warm.exp $ip_workload 50 $basenum
echo "Restarting Sip"
expect -f util/sip.exp $ip_sip

echo "synchronizing config file..."
copy_cfg
echo "Start Overhead Collecting..."
start_oh

start_fault_injectors

sleep 5

if [ $inj_enabled = 'true' ];then
	echo "Injecting faults..."
	python fault_injection/agent.py start
fi

echo "Start OS metric collecting..."
echo "calling servers...."
start_metric_servers
sleep 5
echo "calling client...."
python metric_collect/client.py &
#python metric_collect/client.py 2>/dev/null &
echo "Start Workload..."
echo "making calls.."
# ssh huawei@$ip_workload "source .zshrc;source .zlogin;cd TeleCloud;python workload/excutor/makecall.py"
expect -f util/workload.exp $ip_workload
#ssh huawei@$ip_workload "source .zshrc;source .zlogin;cd TeleCloud;python workload/excutor/makecall.py"

echo "Experiment ends...."

if [ $inj_enabled = 'true' ];then
	echo "End Injecting faults..."
fi

python fault_injection/agent.py stop
sleep 5

# ssh huawei@$ip_sip "sudo service clearwater-sip-stress stop"
echo "End OS metric collecting...."
sh metric_collect/kill_client.sh 2>/dev/null
echo "End Overhead Collecting...."
end_oh 2>/dev/null

sleep $overhead_interval
echo "Stopping Sip"
expect -f util/stop_sip.exp $ip_sip
echo "Copy Overhead Collecting...."
copy_oh
del_oh
copy_recent_sip_xml
