#!/bin/bash

source ./autorun.cfg
source util/util.sh

for tcase in $autorun_case;do
	ts_id=`date +%s`
# 	echo "$tcase normal_test"
# 
# 	sed -i "s/\(topic=\).*/\1${ts_id}-${tcase}_standard/;s/\(benchmark=\).*/\1huawei_2w.bench/;s/\(remapping_startstamp=\).*/\1${ts1}/;s/\(inj_enabled=\).*/\1false/;s/\(f_list=\).*/\1\"\"/" config
# 
# 	if [[ $tcase == *"networkloss"* ]];then
# 		loss=`echo $tcase|sed 's/networkloss/tinyloss/g'`
# 		echo "It's normal NETWORKLOSS, now injecting TINYLOSS!!!"
# 		sed -i "s/\(topic=\).*/\1${ts_id}-${tcase}_standard/;s/\(benchmark=\).*/\1huawei_2w.bench/;s/\(remapping_startstamp=\).*/\1${ts1}/;s/\(inj_enabled=\).*/\1true/;s/\(f_list=\).*/\1\"${loss}\"/" config
# 	fi
# 
# 	if [[ $tcase == *"networklatency"* ]];then
# 		latency=`echo $tcase|sed 's/networklatency/tinylatency/g'`
# 		echo "It's normal NETWORKLATENCY, now injecting TINYLATENCY!!!"
# 		sed -i "s/\(topic=\).*/\1${ts_id}-${tcase}_standard/;s/\(benchmark=\).*/\1huawei_2w.bench/;s/\(remapping_startstamp=\).*/\1${ts1}/;s/\(inj_enabled=\).*/\1true/;s/\(f_list=\).*/\1\"${latency}\"/" config
# 	fi
# 
# 	if [[ $tcase == *"networkcorrupt"* ]];then
# 		corrupt=`echo $tcase|sed 's/networkcorrupt/tinycorrupt/g'`
# 		echo "It's normal NETWORKCORRUPT, now injecting TINYCORRUPT!!!"
# 		sed -i "s/\(topic=\).*/\1${ts_id}-${tcase}_standard/;s/\(benchmark=\).*/\1huawei_2w.bench/;s/\(remapping_startstamp=\).*/\1${ts1}/;s/\(inj_enabled=\).*/\1true/;s/\(f_list=\).*/\1\"${corrupt}\"/" config
# 	fi
# 
# 	sh run.sh
# 
# 	echo "$tcase fault_test"
# 
# 	sed -i "s/\(topic=\).*/\1${ts_id}-${tcase}_fault/;s/\(benchmark=\).*/\1huawei_3d.bench/;s/\(remapping_startstamp=\).*/\1${ts2}/;s/\(inj_enabled=\).*/\1true/;s/\(f_list=\).*/\1\"${tcase}\"/" config
# 	sh run.sh
# 
# 	sleep 60

	echo "$tcase fault_test"

	sed -i "s/\(topic=\).*/\1${ts_id}-${tcase}/;s/\(benchmark=\).*/\1huawei_3h.bench/;s/\(remapping_startstamp=\).*/\1${ts2}/;s/\(inj_enabled=\).*/\1true/;s/\(f_list=\).*/\1\"${tcase}\"/" config
	sh run.sh

	# Restart clearwater since packet corruption may cause some unknown problems
	echo "Restarting Clearwater"
	restart_cleatwater
	sleep 300

	# while [ `ps aux|grep analyze|wc -l` -ge 2 ]
	# do
	# 	sleep 60
	# done

	# sh analyze.sh ${ts_id}-${tcase}
	# sh synWeb.sh ${ts_id}

done
