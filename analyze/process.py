import extract
import sys


if len(sys.argv) < 3:
    print 'use topice \'\' and directory \'results\''
    topic = ''
    directory = 'results'
else:
    topic = sys.argv[1]
    directory = sys.argv[2]


info = extract.extract(topic, directory)

metric_list = []
mask = {}

for exp in info:
    tmp_lst = []
    for line in info[exp]:
        tmp_s = line[-2] + '_' + line[-1]
        tmp_lst.append(tmp_s.strip())
    metric_list.extend(tmp_lst)
    mask[exp] = (tmp_lst)

metric_list = set(metric_list)
for metric in metric_list:
    print "@attribute " + metric + " {TRUE, FALSE}"

info_strip = set(["_".join(term.split('-')[-1].split('_')[0:2]) for term in info.keys()])
s = "@attribute class {"
for exp in info_strip:
    s += exp + ", "
s = s[:-2] + "}"
print s

print ""
print "@data"

for exp in sorted(mask, key=lambda x:int(''.join(x.split('-')[0].split('timestamp')))) :
    s = ""
    for metric in metric_list:
        if metric in mask[exp]:
            s += "TRUE, "
        else:
            s += "FALSE, "
    s += "_".join(exp.split('-')[-1].split('_')[0:2])
    print s
