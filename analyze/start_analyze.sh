#!/bin/bash

if [ $# -lt 1 ];then
	echo "Usage: $0 topic/folder_name"
	exit 1
fi

if [ ! -d data/$1_standard ];then
	echo "folder data/$1_standard not exitsted!!"
	exit 1
fi

if [ ! -d data/$1_fault ];then
	echo "folder data/$1_fault not exitsted!!"
	exit 1
fi

source ./autorun.cfg

tel_dir=`pwd`
rm -r ../SCAPI_data/*
cp data/$1_standard/remapping/* ../SCAPI_data/ 2>/dev/null
cp data/$1_fault/remapping/* ../SCAPI_data/ 2>/dev/null
(cd /opt/IBM/scanalytics/analytics/bin
./stop.sh
expect -f ${tel_dir}/util/cleanup.exp
./udm_converter.sh -deploy ~/workspace/rubis/datasource.pamodel
./admin.sh set_property -i=GrangerDetectionALL aggregation.interval 15
./admin.sh set_property -i=GrangerDetectionALL trainingWindowWeeks 2
./admin.sh set_property -i=GrangerDetectionALL tasp1.errorcount.min 6
./admin.sh set_property -i=GrangerDetectionALL tasp1.errorcount.window 3
./admin.sh set_property -i=SYSTEM metric.retention.days 365
./admin.sh set_property -i=SYSTEM consolidate.alarms.node.enabled true
./start.sh
./admin.sh run_extractor_instance -s 20$ts1-0000 -e 20$ts3-0000)
