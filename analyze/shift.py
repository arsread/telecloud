import os
import sys

if (len(sys.argv)) < 2:
    print "Please specify the exp name"
    sys.exit(1)

exp_ts = sys.argv[1]
files = filter(lambda x: exp_ts in x, os.listdir('./results_alltime'))

experiments = set([x.split('timestamp')[0] for x in files])


def reduc(lines):
	gcdset = []
	res = []
	for line in lines:
		if line.split()[1] not in gcdset:
			res.append(line)
			gcdset.append(line.split()[1])
	return res


for exp in experiments:
	expfiles = filter(lambda x: exp in x, files)
	remain = '-'.join(expfiles[0].split('-')[1:])
	curtime = 0

	while curtime <= 3600:
		content = []
		for i in range(60):
			lines = open('./results_alltime/' + str(exp) + 'timestamp' + '{0:04}'.format(curtime + i * 60) + '-' + str(remain)).readlines()
			content.extend(lines)

		content = reduc(content)

		f = open('./results_60windows/' + exp + 'timestamp' + '{0:04}'.format(curtime) + '-' + str(remain), 'w')
		f.writelines(content)
		f.close()
		curtime += 60
