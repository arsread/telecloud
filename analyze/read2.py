import os
import ConfigParser
import re
import time


config = ConfigParser.RawConfigParser()
config.read('config')
state_dir = config.get('analyze', 'state_dir')
anomalies_dir = config.get('analyze', 'anomalies_dir')
anomaly_string = config.get('analyze', 'anomaly_string')
fault_stamp = config.get('analyze', 'fault_stamp')


def getGCDList():
    gcdlist = {}
    digit_folder = filter(str.isdigit, os.listdir(state_dir))[-1]
    folder = state_dir + '/' + digit_folder + '/GrangerModel/'
    pattern = re.compile('\d+\.json\.\d+\.txt$')
    flist = filter(pattern.search, os.listdir(folder))
    filename = folder + flist[0]
    lines = open(filename).readlines()
    for i in range(len(lines)):
        if lines[i].startswith('Target'):
            gcdnum = int(re.search('\((\d+)\)', lines[i]).group(1))
            gcdlist[gcdnum] = lines[i+1].split()
    return gcdlist


def extractAnomaly():
    anmlist = {}
    anomalies_file = open(
        anomalies_dir + '/' +
        'GrangerDetectionALLFw_Sink_FilteredAlarmStreamSink.log')
    lines = filter(lambda x: anomaly_string in x, anomalies_file.readlines())
    for line in lines:
        line = line.split(',')
        gcdnum = int(line[4])
        if gcdnum not in anmlist:
            anmlist[gcdnum] = []
        anmlist[gcdnum].append(int(line[2]))
    return anmlist


def formatInfo():
    ts_start = int(time.strftime('%s', time.strptime(fault_stamp, "%y%m%d")))

    lines = []
    gcdlist = getGCDList()
    anmlist = extractAnomaly()
    for gcd in anmlist:
        line = []
        line.append(str(len(anmlist[gcd])))
        line.append('GCD' + str(gcd))
        reflex_time = [ts/1000 - ts_start for ts in anmlist[gcd]]
        line.append(reflex_time)
        resource = gcdlist[gcd][0]
        group = gcdlist[gcd][1]
        metric = gcdlist[gcd][2]
        line.extend([group, resource, metric])
        lines.append(line)
    return lines


if __name__ == '__main__':
    lines = formatInfo()
    time_list = []
    for reflex_time in [line[2] for line in lines]:
        time_list.extend(reflex_time)
    time_set = sorted(set(time_list))

    for ts in time_set:
        anomalies = []
        for line in lines:
            if ts in line[2]:
                anomalies.append(line[5])
        print str(ts) + '\t' + " ".join(anomalies)
