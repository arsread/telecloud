#!/bin/bash

(cd $PI_HOME/spl/instances/log/GrangerDetectionALLFw;
curr_info=`grep Overall GrangerDetectionALLFw_stat.log | tail -1 | awk '{print $10$13}'` 2>/dev/null

while [ -z $curr_info ]
do
	sleep 60
	curr_info=`grep Overall GrangerDetectionALLFw_stat.log | tail -1 | awk '{print $10$13}'`
done

prev_info='initial'
while [ $prev_info != $curr_info ]
do
	sleep 60
	prev_info=$curr_info
	curr_info=`grep Overall GrangerDetectionALLFw_stat.log | tail -1 | awk '{print $10$13}'`
done

sleep 60
)

echo "Analyzing Process Finished"
