import os


def extract(topic, folder):
    files_all = os.listdir('./' + folder + '/')
    txtfiles = filter(lambda s: 'txt' in s, files_all)
    files = filter(lambda s: topic in s, txtfiles)

    experiments = {}
    for f in files:
        node, exp = f.strip('.txt').split('_')[-2:]
        lines = open('./' + folder + '/' + f).readlines()
        faults = []
        for l in lines:
            terms = l.split('\t')
            terms[-1] = terms[-1].strip('\n')
            if int(terms[0]) > 0:
                faults.append(terms)
        experiments[f.strip('.txt')] = faults

    return experiments
