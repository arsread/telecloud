import os
import sys

if (len(sys.argv)) < 2:
    print "Please specify the exp name"
    sys.exit(1)

exp_ts = sys.argv[1]
files = filter(lambda x: exp_ts in x, os.listdir('./results2'))

experiments = set([x.split('timestamp')[0] for x in files])

for exp in experiments:
    expfiles = filter(lambda x: exp in x, files)
    timeset = [x.split('-')[0].split('timestamp')[1] for x in expfiles]
    remain = '-'.join(expfiles[0].split('-')[1:])
    curtime = 0
    content = []

    while curtime < 7200:
        if str(curtime) in timeset:
            content = open('./results2/' + str(exp) + 'timestamp' + str(curtime) + '-' + str(remain)).readlines()

        f = open('./results_alltime/' + exp + 'timestamp' + '{0:04}'.format(curtime) + '-' + str(remain), 'w')
        f.writelines(content)
        f.close()
        curtime += 60
