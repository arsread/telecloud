#!/bin/bash

# if [ $# -lt 1 ];then
# 	echo "Usage: $0 topic/folder_name"
# 	exit 1
# fi

# if [ ! -d data/$1_standard ];then
# 	echo "folder data/$1_standard not exitsted!!"
# 	exit 1
# fi
# 
# if [ ! -d data/$1_fault ];then
# 	echo "folder data/$1_fault not exitsted!!"
# 	exit 1
# fi

source ./autorun.cfg

tel_dir=`pwd`

# for file in ../SCAPI_data/*
# do
# 	# rm -r ../SCAPI_data/*
# 	rm -r $file
# done
# 
# for file in data/realtwoweeks2/remapping/*
# do
# 	cp $file ../SCAPI_data/
# done
# 
rm -r ../SCAPI_data/
# cp -r data/realtwoweeks2/remapping/ ../SCAPI_data
cp -r data/$1/remapping ../SCAPI_data
(cd /opt/IBM/scanalytics/analytics/bin
./stop.sh
expect -f ${tel_dir}/util/cleanup.exp
cp -r ${tel_dir}/data/states/ /opt/IBM/scanalytics/analytics/
./udm_converter.sh -deploy ~/workspace/rubis/datasource.pamodel
./admin.sh set_property -i=GrangerDetectionALL aggregation.interval 5
./admin.sh set_property -i=GrangerDetectionALL trainingWindowWeeks 2
./admin.sh set_property -i=GrangerDetectionALL tasp1.errorcount.min 6
./admin.sh set_property -i=GrangerDetectionALL tasp1.errorcount.window 3
./admin.sh set_property -i=SYSTEM metric.retention.days 365
./admin.sh set_property -i=SYSTEM consolidate.alarms.node.enabled true
./start.sh
echo 20$ts2-0000 
echo 20$ts3-0000
./admin.sh run_extractor_instance -s 20$ts2-0000 -e 20$ts3-0000)
