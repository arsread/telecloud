import csv
import sys
import time
import datetime
import ConfigParser

remap = True

config = ConfigParser.RawConfigParser()
config.read('config')

sampling_interval = int(config.get('experiment', 'sampling_interval'))
timegap = int(config.get('experiment', 'remapping_seconds'))
remapping_startstamp = config.get('experiment', 'remapping_startstamp')
start_limit = int(config.get('workload', 'startlimit'))


def getNext(f):
    data = None
    line = f.readline()
    while line != '':
        if 'Total-time' in line:
            time = f.readline().split()[4]

            while line != '':
                if '(limit' in line:
                    limit = line.split()[3].strip(')')
                    break
                line = f.readline()

            while line != '':
                if 'INVITE ---------->' in line:
                    line = f.readline()
                    break
                line = f.readline()

            fail_total = 0
            while line != '':
                lst = line.split()

                if 'post_call_delay' in line:
                    fail_total += int(lst[-1])
                    success = int(lst[-2])
                    break

                if '[' in lst:
                    lst.remove('[')
                if 'E-RTD2' in lst:
                    lst.remove('E-RTD2')
                if 'B-RTD3' in lst:
                    lst.remove('B-RTD3')
                if 'E-RTD3' in lst:
                    lst.remove('E-RTD3')

                fail_total += sum([int(x) for x in lst[3:]])

                line = f.readline()

            total = success + fail_total
            data = [time, limit, total, success]
            break
        line = f.readline()
    return data


def getRate(topic):
    f = open('data/' + topic + '/call_info/sipp.1.out')

    all_info = []
    data = getNext(f)
    time = 0
    nexttime = 0
    while type(data) is list and int(data[1]) < start_limit:
        data = getNext(f)

    time = int(float(data[0]))
    nexttime = time - 1
    data = getNext(f)
    pre_data = [0, 0]
    while data is not None:
        if float(data[0]) > nexttime:
            nexttime = nexttime + 60
            new_data = [int(data[2]), int(data[3])]
            info = [data[0], data[1], new_data[0] - pre_data[0],
                    new_data[1] - pre_data[1]]
            if info[2] == 0:
                if info[3] == 0:
                    info.append(0.0)
                else:
                    info.append(0.0)
            else:
                info.append(info[3]*1.0/info[2])
            pre_data = new_data
            all_info.append(info)

        data = getNext(f)

    f.close()
    return all_info


def csv_stat(topic, startstamp, endstamp):
    start = int(time.strftime('%s', time.strptime(startstamp, "1%y%m%d%H%M%S000")))
    end = int(time.strftime('%s', time.strptime(endstamp, "1%y%m%d%H%M%S000")))
    remapstart = int(time.strftime('%s', time.strptime(remapping_startstamp, "%y%m%d")))

    csvf = open('data/' + topic + '/call_info/rate.csv', 'w')
    writer = csv.writer(csvf,
                        delimiter=',',
                        quotechar='"',
                        quoting=csv.QUOTE_ALL)
    writer.writerow(['TimeStamp', 'Rate'])
    rates = getRate(topic)
    count = 0

    while (start < end):
        t1 = datetime.datetime.fromtimestamp(start).strftime('1%y%m%d%H%M%S000')
        t2 = datetime.datetime.fromtimestamp(start + sampling_interval).strftime('1%y%m%d%H%M%S000')

        t1_remap = datetime.datetime.fromtimestamp(remapstart).strftime('1%y%m%d%H%M%S000')
        t2_remap = datetime.datetime.fromtimestamp(remapstart + sampling_interval).strftime('1%y%m%d%H%M%S000')

        try:
            row = [t1, rates[count][-1]]
        except:
            break
        writer.writerow(row)

        metricfile = open('data/' + topic + '/raw/ClearWater_Stat__' + t1 + '__' + t2 + '.csv', 'w')
        metric_writer = csv.writer(metricfile,
                                   delimiter=',',
                                   quotechar='"',
                                   quoting=csv.QUOTE_ALL)
        metric_writer.writerow(['EndTime', 'Resource', 'SuccessfulRate'])
        metric_writer.writerow([t1, 'CLEARwATER', rates[count][-1]])

        if remap:
            remapfile = open('data/' + topic + '/remapping/ClearWater_Stat__' + t1_remap + '__' + t2_remap + '.csv', 'w')
            remap_writer = csv.writer(remapfile,
                                      delimiter=',',
                                      quotechar='"',
                                      quoting=csv.QUOTE_ALL)
            remap_writer.writerow(['EndTime', 'Resource', 'SuccessfulRate'])
            remap_writer.writerow([t1_remap, 'CLEARwATER', rates[count][-1]])

        start += sampling_interval
        remapstart += timegap
        count += 1


if __name__ == '__main__':
    if len(sys.argv) < 4:
        print "usage:python " + sys.argv[0] + " $topic $starttime $endtime"
        sys.exit(1)
    csv_stat(sys.argv[1], sys.argv[2], sys.argv[3])
