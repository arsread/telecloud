# TeleCloud

TeleCloud: 

- an automatic framework to collect and analyze metrics values, using the runtime of [OpenStack](http://www.openstack.org/) cloud
- deployed at USI - Faculty of Informatics - STAR Laboratory. 

The data collection runs on OpenStack and consists of 

- an open source IMS telecommunication system [Clearwater](http://www.projectclearwater.org/) 
- a workload built on top of the Clearwater [SIPp stress testing](http://clearwater.readthedocs.io/en/stable/Clearwater_stress_testing.html)
- a data analytics component that processes the collected data, using [IBM SmartCloud Predictive Insights](http://www-03.ibm.com/software/products/en/ibm-operations-analytics---predictive-insights).

## Configuration

### Hardware

The physical configuration requires at least 4 physical machines: 

- the **data analytics node** that runs [IBM SmartCloud Predictive Insights](http://www-03.ibm.com/software/products/en/ibm-operations-analytics---predictive-insights) (in our case, version 1.3 that we execute on Redhat 6.3.) to control the experiment and collects data.
- the **workload execution node** that runs Clearwater [SIPp stress testing](http://clearwater.readthedocs.io/en/stable/Clearwater_stress_testing.html) (in our case, on Ubuntu 14.04) to generate the workload. 
- At least two  **OpenStack nodes** that are physical machines that run [OpenStack](http://www.openstack.org/) (in our case we have 8 physical machines that run Ubuntu 14.04.）

### Software

The framework requires following configurations of software deployment:

- OpenStack nodes with the following software packages installed:
	- [OpenStack icehouse](http://docs.openstack.org/icehouse/install-guide/install/apt/content/) 
	- [OpenStack Ceilometer](http://docs.openstack.org/icehouse/install-guide/install/apt/content/ch_ceilometer.html) service
	- *systat 10.0.3-1* and *procinfo 1:2.0.304-1ubuntu1*
	- at least 6 virtual machines in total created by OpenStack, the **Clearwater nodes**. Clearwater nodes run Ubuntu 12.04. Notice that the virtual machines are created on the OpenStack nodes, and their number may be different than the number of OpenStack nodes, and are managed by OpenStack that may not distribute the virtual machines evenly on the nodes. 
  

- Clearwater nodes runs 
	- Clearwater (in our case we used the [manual installation](http://clearwater.readthedocs.io/en/stable/Manual_Install.html)）
	- [SNMP statistics](http://clearwater.readthedocs.io/en/stable/Clearwater_SNMP_Statistics.html?highlight=snmp) service.
	-  *systat 10.0.3-1* and *procinfo 1:2.0.304-1ubuntu1*
   

- The workload execution node with the following software packages installed:
	-  [SIPp stress testing](http://clearwater.readthedocs.io/en/stable/Clearwater_stress_testing.html)


- The data analytics node with the following software packages installed:
	- *tcl 8.5* and *expect 5.45*
	- ssh privilege to access all OpenStack nodes, Clearwater nodes and workload execution node without password with the username **huawei**

## Execution

Executing TeleCloud to collect and analyze metrics values:

### Deploy TeleCloud

To deploy the code, copy the whole source code folder on each node:  the workload execution node, the data analytics node, the Openstack nodes and the Clearwater nodes. In our case, we place the folder under ```~/TeleCloud``` on each node.

### Config Settings

On data analytics node, create a ```config``` file under the root directory of the folder, and specify in it the options needed for the experiments. We also provide a ```config.sample``` file for reference.

The meanings of the options are:

- **[experiment]**
 	- *<topic>* The topic of a individual experiment, which will be used as name of the folder under ```data/```, to store the result of the experiment.
 	- *<benchmark>*  Filename of the workload that will be executed. There are already some workload samples placed under ```workload/generator``` with some figures indicating the plots. 
For example, there is a ```huawei_2w.bench``` file under that folder, which specifies a workload for two weeks. So you can write ```benchmark=huawei_2w``` to use it as the benchmark. You can also [Generate your own workload](#Generate your own workload).
 	- *<sampling>* The time interval in seconds that specifies the data sampling rate (Default value = 60).
 	- *<overhead_interval>* The time interval in seconds that specifies the performance monitoring rate (Default value = 60).
 	- *<remapping_seconds>* The time interval in seconds of the sampling after remapping (Default value = 60). After the end of every experiment, all data during the experiment are remapped to another time period. We do this because it is easier to feed data to IBM SmartCloud Predictive Insights with fixed time period.
 	- *<remapping_startstamp>* The start time of the data after remapping, in the format "140901", to indicate Sep. 1st, 2014.

- **[default]**
 	- *<ips_all>* IPs of all OpenStack nodes and Clearwater nodes.
 	- *<ips_openstack>* IPs of all OpenStack nodes.
 	- *<ips_clearwater>* IPs of all Clearwater nodes.
 	- *<ip_bono>*, *<ip_sprout>*, *<ip_hs>* IPs of related Clearwater nodes.
 	- *<ips_controller>* IPs of the controller node of OpenStack.
 	- *<ips_ceilometer>* IPs of the node of OpenStack Ceilometer.
 	- *<ips_keystone>* IPs of the node of OpenStack KeyStone.
 	- *<ips_ISCAPI>* IPs of the node of OpenStack Ceilometer.
 	- *<ips_snmp>* IPs of the node of snmp nodes, should include bono nodes, sprout nodes, and homestead nodes in order.
 	- *<ips_workload>* IP of the node of the workload execution node.
 	- *<base_dir>* Location of this project on everynode, (Default value = ```TeleCloud```).

- **[workload]**
 	- *<basenum>*, *<cntnum>* Refers to the options ```base``` and ```count``` in the documentation of the [SIPp stress testing](http://clearwater.readthedocs.io/en/stable/Clearwater_stress_testing.html). (Default value = '2010000000' and '1000').
 	- *<availnums>* Maximum count of the available numbers starting from *<basenum>*. (Default value = '100000'}.
 	- *<startlimit>* Numbers of calls in the first interval of the workload. Used to extract successful rate. For example, if your workload starts with 60 calls, you should put 60 here.
 	- *<base>*, *<peak>*, *<wl_days>*, *<wl_start>* Parameters that used to generate workload. *<base>* and *<peak>* specifies the range of the numbers of calls. *<wl_days>* specifies the length of the workload in days, and *<wl_start>* specifies the start day of the week, which should be one of ```Sun```, ```Mon```, ```Tue```, ```Wed```, ```Thu```, ```Fri``` and ```Sat```.
 	- *<call_interval>* Time interval in seconds between calls in benchmark, (Default value  =  900).

- **[keystone]**
 	- *<admin_user>* Admin username in keystone of OpenStack.
 	- *<admin_passwd>* Admin password in keystone of OpenStack.

- **[injection]**
 	- *<ips_injection>* All IPs that will be involved in the fault injection. By default we set it to all IPs of OpenStack nodes and Clearwater nodes.
 	- *<inj_enable>* Flag to turn on/off fault injection, should be either ```true``` or ```false```.
 	- *<f_list>* A string to specify faults to inject. The format is "```<ip of the node to be injected on>-<fault type>_<node name>_<parameter to execute>```". Fault types are place under ```fault_injection/fault_repository/```, and node names are corresponding to the *<xxx_target>* options of this section. For example, *"10.40.7.173-CPUhog_sprout_420"* injects fault on the "sprout" node, which has IP "10.40.7.173". The fault to be inject is ```CPUhog.py``` with parameter ```240``` under ```fault_injection/fault_repository/``` on that node.
 	- *<port>* The port number for fault injection management. (Default value = 9003.)
 	- *<ellis_target>*, *<bono_target>*, ..., *<huawei1_target>*, *<huawei2_target>* ... IPs that specify the IP of the node to be injected on.

- **[analyze]**
 	- *<state_dir>*, *<anomalies_dir>*, *<anomaly_string>* Paths of the baseline model, path of the anomaly logs and a string pattern to recognize an anomalous entry in the log files on the data analytics node.
 	- *<fault_stamp>* Starting time of the analyzing phase. By default, it corresponds to the starting time of the experiment after remapping.
 
### Run the experiment

Run 

```
sh run.sh
```

on the data analytics node

```run.sh``` consist of two scripts:

- **cloudtest.sh** that is used to excute the workload and data collection.
- **data_process.sh** that is used to fetch, re-format and remap the data.

At the end of the execution, the data are placed in ```data/<topic>/``` that contains:

- **raw**: the folder with the raw data
- **call_info**:  the folder with the success rate information
- **overhead**: the folder with the overhead monitor
- **remapping**: the folder with the remapping data

### Analyze the experiment 

Run 

```
sh analyze/start_analyze.sh <topic>
```

on the data analytics node.

To visualize the anomalies produced in the experiments use either the web GUI interface or the log files provided by SCAPI. For more details, see [the manual](https://www.google.ch/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwjMkram5ejMAhWHPxQKHVD6Cy8QFggdMAA&url=https%3A%2F%2Fwww.ibm.com%2Fdeveloperworks%2Fcommunity%2Fwikis%2Fform%2Fanonymous%2Fapi%2Fwiki%2F65114c3c-b36b-49e2-a20e-7840f23183c2%2Fpage%2F9c7fd3d8-7f66-45d5-946b-443e94aaa649%2Fattachment%2F56ebbfad-9583-4556-a743-fbea94383fc2%2Fmedia%2Fpdf_tasp_adminguide_1203.pdf&usg=AFQjCNH0BKEW2NgklDmxg55mjRu4gG9aqw&sig2=ShLxr9EfxDQHQFG-EgwvcQ).

##Generate your own workload

To generate your workload, use the function *weekdata()* in ```workload/generator/weekdata.py```. To call this function, you have to specify the starting day and the length of the workload. It will output a file *benchmark.out*

Additionally, if you want to generate your own plot, you can do it with *straight()* function and *curve()* function in ```workload/generator/workgen.py```
