#!/bin/sh

sh analyze/start_analyze2.sh $1
sh analyze/wait_analyze.sh
cp -r $TASP_HOME/states data/$1/
python analyze/read.py > analyze/results/$1.txt
python analyze/read3.py $1
ts=`echo $1|cut -d - -f 1`
cd analyze
python expand.py $ts
python shift.py $ts
python shift90.py $ts
python shift120.py $ts
python process.py > data.txt
python process.py '' results2 > data2.txt
python process.py '' results_alltime > data_alltime.txt
python process.py '' results_60windows > data_all_60windows.txt
python process.py '' results_120windows > data_all_120windows.txt
python process.py '' results_90windows > data_all_90windows.txt
python process.py $ts results2 > classifier_data/$1.txt
python process.py $ts results_alltime > classifier_data_alltime/$1.txt
python process.py $ts results_60windows > classifier_data_60windows/$1.txt
python process.py $ts results_120windows > classifier_data_120windows/$1.txt
python process.py $ts results_90windows > classifier_data_90windows/$1.txt
