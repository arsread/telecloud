import psutil as ps
import csv
import time
import sys
import signal
import ConfigParser


config = ConfigParser.RawConfigParser()
config.read('config')
interval = int(config.get('experiment', 'overhead_interval'))

flag = True
filename = 'data/overhead/' + time.strftime("%Y-%m-%d_%H:%M") + '.csv'
header = ['timestamp',
        'CPU usage',
        'memory usage',
        'disk read bytes',
        'disk write bytes',
        'network packets received',
        'network packets sent',
        'network error packets received',
        'network error packets sent']

def signal_handler(signal, frame):
    global flag
    flag = False

signal.signal(signal.SIGTERM, signal_handler)

# def monitor(seconds, interval):
#     count = seconds // interval
#     for i in range(count):


def monitor(writer):
    while flag:
        timestamp = int(time.time())
        newrow = []
        newrow.append(timestamp)
        newrow.append(ps.cpu_percent())
        newrow.append(ps.virtual_memory().percent)
        newrow.append(ps.disk_io_counters().read_bytes)
        newrow.append(ps.disk_io_counters().write_bytes)
        newrow.append(ps.net_io_counters().packets_recv)
        newrow.append(ps.net_io_counters().packets_sent)
        newrow.append(ps.net_io_counters().errin)
        newrow.append(ps.net_io_counters().errout)
        writer.writerow(newrow)
        time.sleep(interval)

if __name__ == '__main__':
    ofile = open(filename, 'wb')
    writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
    writer.writerow(header)

    monitor(writer)

    ofile.close()
# if len(sys.argv) < 3:
#     print "Usage:" + sys.argv[0] + " total_seconds interval"
#     sys.exit(1)
# monitor(int(sys.argv[1]), int(sys.argv[2]))
