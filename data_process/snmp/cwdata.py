import sys
import csv
import datetime
import time
import ConfigParser

if len(sys.argv) < 2:
    print "expect 1 argument: $timestamp"
    sys.exit(1)


clsfy = eval('{' + "".join(open('data_process/snmp/snmp.log').readlines()) + '}')

config = ConfigParser.RawConfigParser()
config.read('config')
topic = config.get('experiment', 'topic')
sampling_interval = int(config.get('experiment', 'sampling_interval'))
starttime = sys.argv[1]
startstamp = int(time.strftime('%s',time.strptime(starttime, "1%y%m%d%H%M%S000")))

prefix = 'SNMP_METRIC_'
bono_line = prefix + config.get('default', 'ip_bono') + '\n'
sprout_line = prefix + config.get('default', 'ip_sprout') + '\n'
hs_line = prefix + config.get('default', 'ip_hs') + '\n'


fname = 'data/' + topic + '/raw/' + sys.argv[1] + '_snmp.log'
currenttime = datetime.datetime.fromtimestamp(startstamp).strftime('1%y%m%d%H%M%S000')
currenttime2 = datetime.datetime.fromtimestamp(startstamp + sampling_interval).strftime('1%y%m%d%H%M%S000')
csvname = 'data/' + topic + '/raw/SNMP_GENERAL__' + currenttime + '__' + currenttime2 + '.csv'
csvbononame = 'data/' + topic + '/raw/SNMP_BONO__' + currenttime + '__' + currenttime2 + '.csv'
csvsproutname = 'data/' + topic + '/raw/SNMP_SPROUT__' + currenttime + '__' + currenttime2 + '.csv'
csvhsname = 'data/' + topic + '/raw/SNMP_HOMESTEAD__' + currenttime + '__' + currenttime2 + '.csv'

f = open(fname)
lines = f.readlines()

bono_num = lines.index(bono_line)
sprout_num = lines.index(sprout_line)
hs_num = lines.index(hs_line)

bonolines = lines[bono_num + 1: sprout_num - 1]
sproutlines = lines[sprout_num + 1: hs_num - 1]
hslines = lines[hs_num + 1: -1]

bono_snmp_lines = filter(lambda s:s.startswith('SNMPv2-SMI::enterprises'), bonolines)
sprout_snmp_lines = filter(lambda s:s.startswith('SNMPv2-SMI::enterprises'), sproutlines)
hs_snmp_lines = filter(lambda s:s.startswith('SNMPv2-SMI::enterprises'), hslines)

header = ['EndTime', 'Resource']
for l in max([bono_snmp_lines, sprout_snmp_lines, hs_snmp_lines], key=len):
    if '"' not in l.split('=')[1]:
        header.append(clsfy[l.split('=')[0].strip()])
csvf = open(csvname, 'w')
writer = csv.writer(csvf, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
writer.writerow(header)

for term, name in zip([bono_snmp_lines, sprout_snmp_lines, hs_snmp_lines], ['bono', 'sprout', 'homestead']):
    line = [currenttime, name]
    tmp_dict = {}
    for l in term:
        key = clsfy[l.split('=')[0].strip()]
        try:
            value = l.split('=')[1].split(':')[1].strip()
        except IndexError:
            value = l.split('=')[1].strip()
        tmp_dict[key] = value

    for headerterm in header[2:]:
        if headerterm in tmp_dict:
            line.append(tmp_dict[headerterm])
        else:
            line.append('')
    writer.writerow(line)
csvf.close()


bono_cw_lines = filter(lambda s:s.startswith('PROJECT-CLEARWATER-MIB'), bonolines)
sprout_cw_lines = filter(lambda s:s.startswith('PROJECT-CLEARWATER-MIB'), sproutlines)
hs_cw_lines = filter(lambda s:s.startswith('PROJECT-CLEARWATER-MIB'), hslines)

header = ['EndTime', 'Resource']
bono = [currenttime, 'bono']
for l in bono_cw_lines:
    header.append(l.split('=')[0].split('::')[1].strip())
    try:
            bono.append(l.split('=')[1].split(':')[1].strip())
    except IndexError:
        bono.append(l.split('=')[1].strip().strip('"'))
csvf = open(csvbononame, 'w')
writer = csv.writer(csvf, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
writer.writerow(header)
writer.writerow(bono)
csvf.close()

header = ['EndTime', 'Resource']
sprout = [currenttime, 'sprout']
for l in sprout_cw_lines:
    header.append(l.split('=')[0].split('::')[1].strip())
    try:
        sprout.append(l.split('=')[1].split(':')[1].strip())
    except IndexError:
        sprout.append(l.split('=')[1].strip())
csvf = open(csvsproutname, 'w')
writer = csv.writer(csvf, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
writer.writerow(header)
writer.writerow(sprout)
csvf.close()

header = ['EndTime', 'Resource']
homestead = [currenttime, 'homestead']
for l in hs_cw_lines:
    header.append(l.split('=')[0].split('::')[1].strip())
    try:
        homestead.append(l.split('=')[1].split(':')[1].strip())
    except IndexError:
        homestead.append(l.split('=')[1].strip())
csvf = open(csvhsname, 'w')
writer = csv.writer(csvf, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
writer.writerow(header)
writer.writerow(homestead)
csvf.close()

# if len(lines) != 183:
#     print fname + ":linenum_wrong!skipped"
#     open(csvname, 'w')
#     open(csvbononame, 'w')
#     open(csvsproutname, 'w')
#     open(csvhsname, 'w')
#     sys.exit(1)
#
#
# header = ['EndTime', 'Resource']
# for l in lines[16:54]:
#     header.append(clsfy[l.split('=')[0].strip()])
#
# bono = [currenttime, 'bono']
# for l in lines[16:54]:
#     try:
#         bono.append(l.split('=')[1].split(':')[1].strip())
#     except IndexError:
#         bono.append(l.split('=')[1].strip())
#
# sprout = [currenttime, 'sprout']
# for l in lines[91:129]:
#     try:
#         sprout.append(l.split('=')[1].split(':')[1].strip())
#     except IndexError:
#         sprout.append(l.split('=')[1].strip())
#
# homestead = [currenttime, 'homestead']
# for l in lines[144:182]:
#     try:
#         homestead.append(l.split('=')[1].split(':')[1].strip())
#     except IndexError:
#         homestead.append(l.split('=')[1].strip())
#
# csvlst = [header, bono, sprout, homestead]
# csvf = open(csvname, 'w')
# writer = csv.writer(csvf, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
# for l in csvlst:
#     writer.writerow(l)
# csvf.close()
#
# header = ['EndTime', 'Resource']
# bono = [currenttime, 'bono']
# for l in lines[1:15]:
#     header.append(l.split('=')[0].split('::')[1].strip())
#     try:
#             bono.append(l.split('=')[1].split(':')[1].strip())
#     except IndexError:
#         bono.append(l.split('=')[1].strip())
# csvf = open(csvbononame, 'w')
# writer = csv.writer(csvf, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
# writer.writerow(header)
# writer.writerow(bono)
# csvf.close()
#
# header = ['EndTime', 'Resource']
# sprout = [currenttime, 'sprout']
# for l in lines[56:90]:
#     header.append(l.split('=')[0].split('::')[1].strip())
#     try:
#         sprout.append(l.split('=')[1].split(':')[1].strip())
#     except IndexError:
#         sprout.append(l.split('=')[1].strip())
# csvf = open(csvsproutname, 'w')
# writer = csv.writer(csvf, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
# writer.writerow(header)
# writer.writerow(sprout)
# csvf.close()
#
# header = ['EndTime', 'Resource']
# homestead = [currenttime, 'homestead']
# for l in lines[131:143]:
#     header.append(l.split('=')[0].split('::')[1].strip())
#     try:
#         homestead.append(l.split('=')[1].split(':')[1].strip())
#     except IndexError:
#         homestead.append(l.split('=')[1].strip())
# csvf = open(csvhsname, 'w')
# writer = csv.writer(csvf, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
# writer.writerow(header)
# writer.writerow(homestead)
# csvf.close()
