import datetime
import time
import os
import ConfigParser


config = ConfigParser.RawConfigParser()
config.read('config')

timegap = int(config.get('experiment', 'remapping_seconds'))
benchmark = config.get('experiment', 'benchmark')
topic = config.get('experiment', 'topic')
remap_startstamp = config.get('experiment', 'remapping_startstamp')
sampling_interval = int(config.get('experiment', 'sampling_interval'))
call_interval = int(config.get('workload', 'call_interval'))
keys = ["CPU", "IO", "Network", "VM", "System", "Socket", "SNMP_GENERAL", "SNMP_BONO", "SNMP_SPROUT", "SNMP_HOMESTEAD", 'NEUTRON_TRAFFIC', 'NOVA', 'GLANCE', 'ClearWater']

for key in keys:
    ts = int(time.strftime('%s',time.strptime(remap_startstamp, "%y%m%d")))
    count = (open('workload/generator/' + benchmark).readline().count(',') + 1) * call_interval / sampling_interval

    files = sorted(filter(lambda x: key in x, os.listdir("data/" + topic + "/raw")))
    if len(files) < 0:
        continue

    print "Remapping " + key + ":"

    for f in files:
        tf = datetime.datetime.fromtimestamp(ts).strftime('1%y%m%d%H%M%S000')
        tf2 = datetime.datetime.fromtimestamp(ts + timegap).strftime('1%y%m%d%H%M%S000')
        newname = "_".join(f.split('_')[0:-4]) + \
            "__" + str(tf) + "__" + str(tf2) + ".csv"
        fr = open("data/" + topic + "/raw/" + f, 'r')
        fw = open("data/" + topic + "/remapping/" + newname, 'wb')
        line = fr.readline()
        if line == '':
            print "skip " + f
            ts = ts + timegap
            continue
        fw.write(line)
        oneline = fr.readline()
        while oneline != '':
            line = oneline.split(',')
            fw.write(",".join([tf] + line[1:]))
            oneline = fr.readline()
        ts = ts + timegap
