import subprocess
import csv
import datetime
import time
import sys
import os
import ConfigParser


if len(sys.argv) < 2:
    print "expect 2 argument: $startstamp $endstamp"
    sys.exit(1)


config = ConfigParser.RawConfigParser()
config.read('config')
topic = config.get('experiment', 'topic')
sampling_interval = int(config.get('experiment', 'sampling_interval'))
admin = config.get('keystone', 'admin_user')
passwd = config.get('keystone', 'admin_passwd')
ip_ceilometer = config.get('default', 'ip_ceilometer')
ip_keystone = config.get('default', 'ip_keystone')

startstamp = sys.argv[1]
endstamp = sys.argv[2]
start = int(time.strftime('%s', time.strptime(startstamp, "1%y%m%d%H%M%S000")))
end = int(time.strftime('%s', time.strptime(endstamp, "1%y%m%d%H%M%S000")))

starttime = datetime.datetime.fromtimestamp(start).strftime('%Y-%m-%dT%H:%M:%S')
endtime = datetime.datetime.fromtimestamp(end).strftime('%Y-%m-%dT%H:%M:%S')
starttimestamp = datetime.datetime.fromtimestamp(start).strftime('%s')

datapath = 'data/' + topic + '/raw/'
FNULL = open(os.devnull, 'w')
group = {}

group['NOVA'] = ['cpu', 'cpu_util', 'disk.ephemeral.size', 'disk.read.bytes', 'disk.read.bytes.rate', 'disk.read.requests', 'disk.read.requests.rate', 'disk.root.size', 'disk.write.bytes', 'disk.write.bytes.rate', 'disk.write.requests', 'disk.write.requests.rate', 'instance ', 'instance:m1.small', 'vcpus', 'memory']
group['GLANCE'] = ['image', 'image.delete', 'image.download', 'image.serve', 'image.size', 'image.update', 'image.upload']
group['NEUTRON_IP'] = ['ip.floating', 'ip.floating.create', 'ip.floating.update']
group['NEUTRON_MANAGE'] = ['network ', 'network.create']
group['NEUTRON_TRAFFIC'] = ['network.incoming.bytes', 'network.incoming.bytes.rate', 'network.incoming.packets', 'network.incoming.packets.rate', 'network.outgoing.bytes', 'network.outgoing.bytes.rate', 'network.outgoing.packets', 'network.outgoing.packets.rate']
group['NEUTRON_PORT'] = ['port', 'port.create']
group['NEUTRON_ROUTER'] = ['router', 'router.create', 'router.update']
group['NEUTRON_SUBNET'] = ['subnet', 'subnet.create', 'subnet.update']


def reqtoken():
    curl = ['curl', '-i', '-H', 'Content-Type: application/json', '-d',
            '{ "auth": { "identity": { "methods": ["password"], "password": { "user": { "name": "' + admin + '",' +
            '"domain": { "id": "default"  }, "password": "' + passwd + '" } } } } }',
            'http://' + ip_keystone + ':5000/v3/auth/tokens']
    tokenline = subprocess.Popen(curl, stdout=subprocess.PIPE, stderr=FNULL).communicate()[0].split('\n')[1]
    token = tokenline.split(':')[1].strip()
    return token


token = reqtoken()
meterlst = ''
while meterlst == 'Authentication required' or meterlst == '':
    token = reqtoken()
    curl = ['curl', '-H', 'X-Auth-Token: ' + token,  'http://' + ip_ceilometer + ':8777/v2/meters']
    meterlst = subprocess.Popen(curl, stdout=subprocess.PIPE, stderr=FNULL).communicate()[0]
meters = eval(meterlst.replace('null', 'None'))

# print [ term['name'] for term in meters ]

resources = {}
for key in group:
    lst = []
    for metric in group[key]:
        for term in meters:
            if term['name'] == metric:
                lst.append(term['resource_id'])
    lst_uniq = list(set(lst))
    resources[key] = lst_uniq

for key in group:
    print 'Processing Ceilometer Group:' + key
    groupdata = {}
    for metric in group[key]:
        for resource in resources[key]:
            out = ''
            while out == 'Authentication required' or out == '':
                token = reqtoken()
                q = '[{"field": "timestamp", \
                    "op": "ge", \
                    "value": "' + starttime + '"}, \
                    {"field": "timestamp", \
                    "op": "lt", \
                    "value": "' + endtime + '"}, \
                    {"field": "resource_id", \
                    "op": "eq", \
                    "value": "' + resource + '"}]'
                token_option = 'X-Auth-Token: ' + token
                content_option = 'Content-Type: application/json'
                ceilometer_address = 'http://' + ip_ceilometer + ':8777/v2/meters/' + metric.strip() + '/statistics'
                q_option = '{"q" :' + q + ', "period": ' + str(sampling_interval) + '}'
                curl = ['curl', '-X', 'GET', '-H', token_option, '-H', content_option,
                        '-d', q_option, ceilometer_address]
                out = subprocess.Popen(curl, stdout=subprocess.PIPE, stderr=FNULL).communicate()[0]

            data = [str(term['avg']) for term in eval(out.replace('null', 'None'))]
            for i in range(len(data)):
                currenttime = datetime.datetime.fromtimestamp(int(starttimestamp) + i * 10).strftime('1%y%m%d%H%M%S000')
                if not groupdata.has_key(currenttime):
                    groupdata[currenttime] = {}
                if not groupdata[currenttime].has_key(resource):
                    groupdata[currenttime][resource] = {}
                groupdata[currenttime][resource][metric.strip()] = data[i].strip()
    for timestp in sorted(groupdata):
        f = open( datapath + 'OPENSTACK_' + key + '__' + str(timestp) + '__' + str(int(timestp) + 10000) + '.csv', 'w')
        writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        header = ['EndTime', 'Resource']
        for metric in group[key]:
            header.append(metric)
        writer.writerow(header)
        for resource in groupdata[timestp]:
            line = []
            line.append(timestp)
            line.append(resource)
            for metric in group[key]:
                try:
                    line.append(groupdata[currenttime][resource][metric.strip()])
                except KeyError:
                    line.append('')
            writer.writerow(line)
        f.close()
