import random


def straight(base, duration, ran=0.1):
    work = []
    for i in range(duration):
        val = base + random.randint(-int(base*ran), int(base * ran))
        work.append(max(val, 0))
    return work


def curve(begin, end, duration, ran=0.1):
    work = []
    k = (end - begin) * 1.0 / duration
    for i in range(duration):
        base = int(begin + k * i)
        val = base + random.randint(-int(base*ran), int(base * ran))
        work.append(max(val, 0))
    return work


def gen(lst=['straight(20, 100)']):
    workload = []
    for sec in lst:
        workload.extend(eval(sec))
    return workload
