import matplotlib.pyplot as plt
import workgen
import ConfigParser


config = ConfigParser.RawConfigParser()
config.read('config')

call_interval = int(config.get('workload', 'call_interval'))
remapping_seconds = int(config.get('experiment', 'remapping_seconds'))
day_counts = 60 * 60 * 24 / remapping_seconds
p1 = str(day_counts / 3)
p2 = str(day_counts / 4)
p3 = str(day_counts - int(p1) - int(p2) * 2)

base = int(config.get('workload', 'base'))
peak = int(config.get('workload', 'peak'))
base_weekend = base
peak_weekend = peak * 2 / 3
weekdays = ['straight(' + str(base) + ', ' + p1 + ')',
            'curve(' + str(base) + ',' + str(peak) + ', ' + p2 + ')',
            'curve(' + str(peak) + ',' + str(base) + ', ' + p2 + ')',
            'straight(' + str(base) + ', ' + p3 + ')']
weekends = ['straight(' + str(base_weekend) + ' ,' + p1 + ')',
            'curve(' + str(base_weekend) + ',' + str(peak_weekend) + ', ' + p2 + ')',
            'curve(' + str(peak_weekend) + ',' + str(base_weekend) + ', ' + p2 + ')',
            'straight(' + str(base_weekend) + ', ' + p3 + ')']
weekdic = {'Sun': 6, 'Mon': 0, 'Tue': 1, 'Wen': 2,
           'Thu': 3, 'Fri': 4, 'Sat': 5}


days = int(config.get('workload', 'wl_days'))
start = config.get('workload', 'wl_start')


def plot_and_save(workload):
    x = [i for i in range(len(workload))]
    plt.ylim(0, max(workload) * 1.5)
    plt.plot(x, workload)
    plt.show()
    # plt.savefig('benchmark.png')
    f = open( 'workload/generator/benchmark.out', 'w')
    f.write(str(workload))
    f.close()


def weekdata():
    global days
    global start
    count = weekdic[start]
    data = []
    for day in range(days):
        if count < 5:
            bench = weekdays
        else:
            bench = weekends
        data.extend(workgen.gen(bench))
        count = (count + 1) % 7
    return data

if __name__ == '__main__':
    plot_and_save(weekdata())
