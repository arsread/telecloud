import sys
import ConfigParser
import matplotlib.pyplot as plt
import random

if len(sys.argv) < 3:
    print "expect arguments: $inputfile $outputfile"
    sys.exit(1)

config = ConfigParser.RawConfigParser()
config.read('../../config')
peak = int(config.get('workload', 'peak'))

workload = open(sys.argv[1]).readline().split('\r')
lst = []
maxi = max([int(line) for line in workload])
h_scale = int(0.15 * peak)
l_scale = int(0.05 * peak)
count = 0
sigh = random.randint(0, 1)
for num in workload:
    count += 1
    if count % 48 == 0:
        sigh = random.randint(0, 1)
    if sigh == 0:
        number = max(0, int(int(num)*peak/maxi + random.randint(l_scale, h_scale)))
    else:
        number = max(0, int(int(num)*peak/maxi + random.randint(-l_scale, 0)))
    lst.append(number)

x = [i for i in range(len(lst))]
plt.ylim(0, max(lst) * 1.5)
plt.plot(x, lst)
plt.show()

of = open(sys.argv[2], 'w')
of.write(str(lst))
of.close()
