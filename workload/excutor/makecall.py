# import subprocess
import time
# import os
import ConfigParser
import socket


config = ConfigParser.RawConfigParser()
config.read('config')

basenum = int(config.get('workload', 'basenum'))
cntnum = int(config.get('workload', 'cntnum'))
call_interval = int(config.get('workload', 'call_interval'))
availnums = int(config.get('workload', 'availnums'))
ip_ISACPI = config.get('default', 'ip_ISCAPI')
benchmark = config.get('experiment', 'benchmark')
topic = config.get('experiment', 'topic')
benfilename = 'workload/generator/' + benchmark
outname = 'data/' + 'call_info/' + topic + '_success_rate.out'

ip_sip = config.get('default', 'ip_sip')


def run():
    benfile = open(benfilename, 'r')
    ben = eval(benfile.readline())
    benfile.close()
#    out = open(outname, 'w')
#    p_pool = []
    count = 0
    pre_call = 0
    for num in ben:
        utime = time.time()
        print "Round " + str(count) + ": " + str(num) + " Calls"
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sym = (num - pre_call) >= 0 and '+' or '-'
        for i in range(abs(num - pre_call)):
            sock.sendto(sym, (ip_sip, 8888))
            time.sleep(0.05)
#        base = basenum + cntnum * 2 * (count % (availnums/2/cntnum))
#        p = subprocess.Popen(["sh", "workload/excutor/makecall.sh", str(num), str(base)],
#                             stdout=subprocess.PIPE)
#        p_pool.append(p)
        count += 1
        pre_call = num
        time.sleep(max(0, call_interval - (time.time() - utime)))

#    os.system("ssh scadmin@" + ip_ISCPI + " sh /home/scadmin/collect/metric/kill_client.sh")
#    os.system("ssh scadmin@" + ip_ISCPI + " source /home/scadmin/collect/metric/util.sh;end_oh")

#    print "Writing success rate..."
#    for p in p_pool:
#        out.write(p.communicate()[0])
#
#    out.close()


if __name__ == '__main__':
    run()
