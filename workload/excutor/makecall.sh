#!/bin/sh

if [ $# -lt 2 ];then
	echo "Usage: $0 number_of_calls start_num"
	exit 1
fi

ruby workload/excutor/uas-tcp.rb $1 $2> /dev/null &
out="$(ruby workload/excutor/uac-tcp.rb $1 $2)"

echo 'Successful:'`echo $out|sed 's/\ /\n/g'|grep Successful|wc -l`
echo 'Failed:'`echo $out|sed 's/\ /\n/g'|grep Failed|wc -l`
