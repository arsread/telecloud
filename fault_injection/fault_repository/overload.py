#!/usr/bin/python

import time
import ConfigParser
import socket
import sys
import random

if len(sys.argv) < 3:
    print("expect 2 argument: sip $interval")
    sys.exit(1)
elif sys.argv[1] != 'sip':
    print("expect 2 argument: sip $interval")
    sys.exit(1)

_random = False
exponecial = False
para = sys.argv[2]

print(para)
if 'random' in para:
    _random = True
    para = para.replace('random', '')

if 'expo' in para:
    exponecial = True
    para = para.replace('expo', '')

try:
    interval = int(para)
except ValueError:
    print("interval cannot be parsed!!Existing..")
    sys.exit(1)

config = ConfigParser.RawConfigParser()
config.read('config')
ip_sip = config.get('default', 'ip_sip')

expo_scale = 1
while 1:
    if _random:
        if random.random() > 0.5:
            continue

    if exponecial:
        expo_scale += 1

    for i in range(expo_scale):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto('*', (ip_sip, 8888))

    time.sleep(interval)
