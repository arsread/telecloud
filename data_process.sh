#!/bin/bash

if [ ! -f config ];then
	echo "config not exist!"
	exit 1
fi


source ./config 2 >/dev/null
source util/util.sh 2>/dev/null

if [ ! -d data/$topic ];then
	echo "experiment not exist!"
	exit 1
fi

starttime=`ls data/$topic/raw/| grep csv | awk -F'__' '{print $2}' | sort | uniq | head -1`
endtime=`ls data/$topic/raw/| grep csv | awk -F'__' '{print $2}' | sort | uniq | tail -1`

echo "Time infomation"
echo $starttime
echo $endtime
echo "Fetching Ceilometer Data..."
# echo "Skip fetching due to faulty database."
python data_process/ceilometer/ceilometer.py $starttime $endtime

echo "Extracting Successful Rate $topic $starttime $endtime..."
python analyze/sipstat.py $topic $starttime $endtime

echo ""
echo "Remapping Data..."
python data_process/time-remapping/postpro.py
