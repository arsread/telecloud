#!/bin/sh

source ./config
ips=$ips_all
ips_2=$ips_openstack
# ips="10.40.7.151 10.40.7.152 10.40.7.150 10.40.7.153 10.40.7.171 10.40.7.172 10.40.7.173 10.40.7.174 10.40.7.175 10.40.7.176"
# ips_2="10.40.7.151 10.40.7.152 10.40.7.150 10.40.7.153"

function start_oh(){
	for ip in $ips;do
		echo "start overhead $ip"
		ssh huawei@$ip -n "cd ${base_dir};nohup python performance_monitor/overhead/overhead.py >data/overhead/nohup.log 2>&1 &"
	done
}

function end_oh(){
	for ip in $ips;do
		echo "end overhead "$ip
		ssh huawei@$ip "sh ${base_dir}/performance_monitor/overhead/kill_oh.sh"
	done
}

function copy_oh(){
	for ip in $ips;do
		timeout 30 scp "huawei@$ip:${base_dir}/data/overhead/*.csv" ./data/$topic/overhead/$ip.csv
	done
}

function copy_suc_rate(){
	scp "huawei@${ip_workload}:${base_dir}/data/call_info/${topic}_success_rate.out" ./data/$topic/call_info/
}

function copy_recent_sip_xml(){
	name=`ssh huawei@$ip_sip "ls -t /var/log/clearwater-sip-stress/*\_.csv|head -n 1"`
	name_sip=`ssh huawei@$ip_sip "ls -t /var/log/clearwater-sip-stress/*.out|head -n 1"`
	scp huawei@$ip_sip:$name data/$topic/call_info/
	scp huawei@$ip_sip:$name_sip data/$topic/call_info/
}

function del_oh(){
	for ip in $ips;do
		ssh huawei@$ip -n "cd ${base_dir}/data/overhead;rm *.log *.csv"
	done
}

function start_metric_servers(){
	for ip in $ips_2;do
		echo "start metric "$ip
		ssh huawei@$ip "cd ${base_dir};nohup python metric_collect/server.py >nohup_oscollect.log 2>&1 &"
	done
}

function start_fault_injectors(){
	for ip in $ips_injection;do
		echo "start injectors "$ip
		ssh huawei@$ip "cd ${base_dir};nohup python fault_injection/injector.py >nohup_injector.log 2>&1 &"
	done
}

function memlk(){
	if [ $# -lt 1 ];then
		echo "Usage:memlk ip"
	else
		ssh huawei@$1 -n "nohup collect/memleak >/dev/null 2>&1 &"
	fi
}

function copy_cfg(){
	for ip in $ips;do
		echo "sychronizing cfg $ip"
		scp config huawei@$ip:$base_dir/
	done
	echo "sychronizing cfg $ip_workload"
	scp config huawei@$ip_workload:$base_dir/
}

function restart_cleatwater(){
	for ip in $ips_clearwater;do
		echo "restarting "$ip
		ssh huawei@$ip -n "sudo monit restart all"
	done
}

function deploy(){
	for ip in $ips_all;do
		ssh huawei@$ip $*
	done
}
