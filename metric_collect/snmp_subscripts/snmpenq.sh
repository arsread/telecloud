#!/bin/sh

source ./config 2>/dev/null

if [ $# -lt 1 ];then
        echo "Usage: $0 time"
        exit 1
fi

for ip in $ip_snmp
do
	echo "SNMP_METRIC_"$ip >> data/$topic/raw/$1_snmp.log
	snmpwalk -c clearwater -m metric_collect/snmp_subscripts/PROJECT-CLEARWATER-MIB -v2c $ip .1 .2 >> data/$topic/raw/$1_snmp.log
done

python data_process/snmp/cwdata.py $1

# rm data/$topic/raw/$1_snmp.log
