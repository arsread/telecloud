#!/bin/sh

if [ $# -lt 1 ];then
	echo "Usage: $0 topic/folder_name"
	exit 1
fi

mkdir $1
mkdir $1/os
mkdir $1/snmp
mkdir $1/overhead
mkdir $1/ceilometer
mkdir $1/post_data
mv ./*.csv $1/os
mv ./*.log $1/snmp/
source ./util.sh
copy_and_del_oh
mv ./*.csv $1/overhead
