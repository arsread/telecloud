#!/usr/bin/env python

import socket
from os_subscipts.monitor import monitor


TCP_IP = ''
TCP_PORT = 5005
BUFFER_SIZE = 1024

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

conn, addr = s.accept()
print 'Connected to address:', addr
while 1:
    try:
        data = conn.recv(BUFFER_SIZE)
        if not data:
            break
        if data == "collect":
            print "sending data.."
            conn.send(str(monitor()))  # echo
        if data == "stop":
            break
    except:
        break
conn.close()
