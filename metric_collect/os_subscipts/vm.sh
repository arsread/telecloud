#!/bin/bash

swp=`free | grep Swap | awk '{print $3/$2*100}'`
used=`free -m|grep '-' | awk '{print $3}'`
total=`free -m|grep 'Mem' | awk '{print $2}'`
# percent=$(($used * 100 / $total))
percent=`echo "scale=8; $used*100/$total" | bc`
mem=`free | grep Mem | awk '{print $6/$2*100"\t"$7/$2*100}'`

echo $swp" "$percent" "$mem
