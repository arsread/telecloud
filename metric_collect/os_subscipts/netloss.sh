#!/bin/sh

source ./config 2>/dev/null
. ./config 2>/dev/null

main=`ping -c 20 -i 0.2 -W $ping_intervals 10.20.0.14 | grep loss | sed 's/.*\ \([0-9]\+\)%\ packet\ loss.*/\1/g'`
echo ${main}
