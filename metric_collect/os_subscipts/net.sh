#!/bin/sh

source ./config 2>/dev/null
. ./config 2>/dev/null

main=`sar -n DEV $ping_intervals 1|grep eth0|tail -n 1 | awk '{print $5*1024"\t"$6*1024"\t"}'`
echo ${main}
