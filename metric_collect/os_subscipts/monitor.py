import subprocess
import platform
import psutil as ps
import time


def monitor():
    nodename = platform.node()

    metrics = {}

    proc_sys = subprocess.Popen(['metric_collect/os_subscipts/sys.sh'], stdout=subprocess.PIPE)
    proc_net = subprocess.Popen(['metric_collect/os_subscipts/net.sh'], stdout=subprocess.PIPE)
    proc_net2 = subprocess.Popen(['metric_collect/os_subscipts/net2.sh'], stdout=subprocess.PIPE)
    proc_netloss = subprocess.Popen(['metric_collect/os_subscipts/netloss.sh'], stdout=subprocess.PIPE)
    proc_vm = subprocess.Popen(['metric_collect/os_subscipts/vm.sh'], stdout=subprocess.PIPE)
    proc_sock = subprocess.Popen(['metric_collect/os_subscipts/sock.sh'], stdout=subprocess.PIPE)
    proc_io = subprocess.Popen(['metric_collect/os_subscipts/io.sh'], stdout=subprocess.PIPE)

    recv1 = ps.net_io_counters().packets_recv
    sent1 = ps.net_io_counters().packets_sent
    time.sleep(5)
    recv2 = ps.net_io_counters().packets_recv
    sent2 = ps.net_io_counters().packets_sent

    time.sleep(10)

    if proc_sys.poll() is None:
        cpu = ['0', '0', '0', '0']
        sys = ['0', '0', '0', '0', '0', '0']
        try:
            proc_sys.terminate()
        except OSError:
            pass
    else:
        out_sys = proc_sys.communicate()[0].split('\n')
        cpu = out_sys[0].split()
        sys = out_sys[1].split()

    metrics['cpu'] = [nodename] + cpu
    metrics['sys'] = [nodename] + sys

    if proc_net.poll() is None:
        out_net = ['0.00']
        try:
            proc_net.terminate()
        except OSError:
            pass
    else:
        out_net = proc_net.communicate()[0].split('\n')

    if proc_net2.poll() is None:
        out_net2 = ['0 0']
        try:
            proc_net2.terminate()
        except OSError:
            pass
    else:
        out_net2 = proc_net2.communicate()[0].split('\n')

    # Not terminated
    if proc_netloss.poll() is None:
        out_netloss = ['100']
        try:
            proc_netloss.terminate()
        except OSError:
            pass
    else:
        loss_result = proc_netloss.communicate()[0]
        out_netloss = loss_result.split('\n')

    net = out_net[0].split()
    net2 = out_net2[0].split()
    netloss = out_netloss[0].split()
    net.extend(net2)
    net.append(recv2 - recv1)
    net.append(sent2 - sent1)
    net.extend(netloss)
    metrics['net'] = [nodename] + net

    out_vm = proc_vm.communicate()[0].split('\n')
    vm = out_vm[0].split()
    metrics['vm'] = [nodename] + vm

    out_sock = proc_sock.communicate()[0].split('\n')
    sock = out_sock[0].split()
    metrics['sock'] = [nodename] + sock

    out_io = proc_io.communicate()[0].split('\n')
    io = out_io[0].split()
    metrics['io'] = [nodename] + io

    return metrics


if __name__ == '__main__':
    print monitor()
