#!/bin/sh

iostat -x 1 2 | grep da | tail -n 1 | awk '{print $1"\t"$10"\t"$9"\t"$6*1024"\t"$7*1024}'
