#!/bin/sh

ps=`getconf PAGESIZE`
vm=`vmstat 1 2 | tail -n 1`
cpu=`echo $vm | awk '{print $13"\t"$14"\t"(100-$15)"\t"$16}'`
swap=`echo $vm | awk -v ps=$ps '{print $7"\t"$8}'`
cxt=`echo $vm | awk -v ps=$ps '{print $12}'`
pf1=`ps -A -o pid,min_flt,maj_flt,cmd | awk 'BEGIN{SUM=0} {SUM=SUM+$2} END{print SUM}'`
po1=`ps -A -o pid,min_flt,maj_flt,cmd | awk 'BEGIN{SUM=0} {SUM=SUM+$3} END{print SUM}'`
sleep 1
pf2=`ps -A -o pid,min_flt,maj_flt,cmd | awk 'BEGIN{SUM=0} {SUM=SUM+$2} END{print SUM}'`
po2=`ps -A -o pid,min_flt,maj_flt,cmd | awk 'BEGIN{SUM=0} {SUM=SUM+$3} END{print SUM}'`
pf=$(( $pf2-$pf1 ))
po=$(( $po2-$po1 ))
pn=$((`ps -e | wc -l`-1 ))
echo $cpu
echo $cxt" "$po" "$swap" "$pf" "$pn
