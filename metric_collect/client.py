#!/usr/bin/env python

import socket
import traceback
import sys
import time
import csv
import os
import re
import signal
import ConfigParser


flag = True


def signal_handler(signal, frame):
    global flag
    flag = False


signal.signal(signal.SIGTERM, signal_handler)

config = ConfigParser.RawConfigParser()
config.read('config')


# TCP_IP = ['10.40.7.151',
#           '10.40.7.152',
#           '10.40.7.150',
#           '10.40.7.153']

TCP_IP = config.get('default', 'ips_openstack').strip('"').split(' ')
topic = config.get('experiment', 'topic')
base_dir = config.get('default', 'base_dir')
sampling_interval = int(config.get('experiment', 'sampling_interval'))

filepath = 'data/' + topic + '/raw/'
GROUPS = {'cpu': 'KLZ_CPU',
          'net': 'KLZ_Network',
          'sys': 'KLZ_System_Statics',
          'vm': 'KLZ_VM_Stats',
          'sock': 'KLZ_Sockets_Status',
          'io': 'KLZ_IO_Ext'}
metricname = {}
metricname['cpu'] = ['EndTime',
                     'Resource',
                     'User_CPU',
                     'System_CPU',
                     'Busy_CPU',
                     'Wait_IO_CPU']
metricname['net'] = ['EndTime',
                     'Resource',
                     'Bytes_received_per_sec',
                     'Bytes_send_per_sec',
                     'Retransmission',
                     'Pkts_received_5s_python',
                     'Pkts_sent_5s_python',
                     'Loss_rate_5s']
metricname['sys'] = ['EndTime',
                     'Resource',
                     'Cxt_Switches_per_sec',
                     'Pages_paged_out_per_sec',
                     'Pages_Swapped_in',
                     'Pages_Swapped_out',
                     'Pages_Faults_per_sec',
                     'Total_Number_Processes']
metricname['vm'] = ['EndTime',
                    'Resource',
                    'Swap_Space_Used',
                    'Memory_Used',
                    'Memory_in_Buffers',
                    'Memory__Cached']
metricname['sock'] = ['EndTime',
                      'Resource',
                      'Sockets_in_use']
metricname['io'] = ['EndTime',
                    'Resource',
                    'Device_Name',
                    'Avg_wait_time',
                    'Avg_req_queue_length',
                    'Read_bytes_per_sec',
                    'Write_bytes_per_sec']
TCP_PORT = 5005
BUFFER_SIZE = 4096

skts = []
ip_map = {}
for ip in TCP_IP:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ip_map[s] = ip
    print "connecting: " + ip
    try:
        s.connect((ip, TCP_PORT))
    except:
        print "Socket connection " + ip_map[s] + " Error!"
        sys.exit(1)
    skts.append(s)

while flag:
    timestamp = time.strftime("1%y%m%d%H%M%S000", time.gmtime())
    utime = time.time()
    data = []
    for s in skts:
        try:
            s.send("collect")
        except socket.error:
            print "Socket " + ip_map[s] + " error while sending!"
            print traceback.print_exc()

    time.sleep(20)

    for s in skts:
        try:
            s.settimeout(3.0)
            recv_data = s.recv(BUFFER_SIZE)
            s.settimeout(None)
            if recv_data:
                data.append(recv_data)
        except socket.timeout:
            print "Socket " + ip_map[s] + " Timeout!"
            s.settimeout(None)
        except:
            print "Socket " + ip_map[s] + " error while recving!"
            print traceback.print_exc()
            sys.exit(1)
    for mtr in GROUPS:
        filename = filepath + GROUPS[mtr] + \
            "__" + \
            str(timestamp) + \
            "__" + \
            str(int(timestamp) + 10000) + \
            ".csv"
        f = open(filename, 'wb')
        writer = csv.writer(f,
                            delimiter=',',
                            quotechar='"',
                            quoting=csv.QUOTE_ALL)
        writer.writerow(metricname[mtr])
        for d in data:
            patterns = re.findall('{[^{]*}', d)
            if len(patterns) > 0:
                if len(patterns) > 1:
                    print "received more than one data: " + str(patterns)
                dic = eval(patterns[-1])
                writer.writerow([str(timestamp)] + dic[mtr])
        f.close()
    os.system("sh metric_collect/snmp_subscripts/snmpenq.sh " + timestamp)
    print "Done collection round.."
    time.sleep(max(0, sampling_interval - (time.time() - utime)))

print "closing sockets..."
for s in skts:
    s.close()
