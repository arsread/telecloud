#!/bin/sh

mkdir ~/.ssh
apt-get update
apt-get install -y git ctags tmux zsh openvswitch-switch
apt-get install -y sysstat procinfo expect bc make gcc
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
cd ~/.vim/bundle
git clone git://github.com/altercation/vim-colors-solarized.git
mkdir ~/.vim/colors
cp ~/.vim/bundle/vim-colors-solarized/colors/solarized.vim ~/.vim/colors
cd ~
chown -R huawei:huawei .vim .ssh .oh-my-zsh
wget --no-check-certificate https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | sh
rm -f .zshrc
