#!/bin/bash

if [ $# -lt 1 ];then
	echo "Usage:$0 number"
	exit 1
fi


for i in 1 2 3 4 5 6 7 8 9 10 11 12
do
	ovs-vsctl del-port gre$i
done

ovs-vsctl add-br br-int0
ovs-vsctl set bridge br-int0 stp_enable=true

for i in 1 2 3 4
do
	if [ $i -ne $1 ];then
		if [ $i -eq 1 ];then ip=1;fi;
		if [ $i -eq 2 ];then ip=2;fi;
		if [ $i -eq 3 ];then ip=0;fi;
		if [ $i -eq 4 ];then ip=3;fi;
		j=$1
		num=$((i*j)) 
		ovs-vsctl add-port br-int0 gre$i
		ovs-vsctl set interface gre$i type=gre options:remote_ip=10.40.7.15$ip
	fi
done

ovs-vsctl add-port br-int0 p1 tag=1
ovs-vsctl set interface p1 type=internal
ovs-vsctl add-port br-int0 p2 tag=2
ovs-vsctl set interface p2 type=internal

echo "
auto p1
iface p1 inet static
        address 192.168.1.1$1
	netmask 255.255.255.0" >> /etc/network/interfaces

echo "
auto p2
iface p2 inet static
        address 192.168.2.1$1
	netmask 255.255.255.0" >> /etc/network/interfaces

ifconfig p1 mtu 1400
ifconfig p2 mtu 1400
