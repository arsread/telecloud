#!/bin/sh

if [ $# -lt 1 ];then
	echo "Usage $0 command"
	exit 1
fi

service ceilometer-agent-central $1
service ceilometer-agent-notification $1
service ceilometer-api $1
service ceilometer-collector $1
service ceilometer-alarm-evaluator $1
service ceilometer-alarm-notifier $1
