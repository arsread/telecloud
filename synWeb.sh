cd analyze
scp data*.txt huawei@10.20.0.13:huawei_experiment/
scp results/*$1* huawei@10.20.0.13:huawei_experiment/results/
scp results2/*$1* huawei@10.20.0.13:huawei_experiment/results2/
scp classifier_data/*$1* huawei@10.20.0.13:huawei_experiment/classifier_data/
scp classifier_data_alltime/*$1* huawei@10.20.0.13:huawei_experiment/classifier_data_alltime/
scp classifier_data_60windows/*$1* huawei@10.20.0.13:huawei_experiment/classifier_data_60windows/
scp classifier_data_90windows/*$1* huawei@10.20.0.13:huawei_experiment/classifier_data_90windows/
scp classifier_data_120windows/*$1* huawei@10.20.0.13:huawei_experiment/classifier_data_120windows/
